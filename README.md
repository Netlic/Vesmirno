# Vesmirnô
PHP based game.

## Command composition
`god:create --o=SpaceTime`
 - `god`    - command entity
 - `create` - command's entity defined activity
 - `o` - shortcut for activity parameter
 - `SpaceTime` - actual parameter value
