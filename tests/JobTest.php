<?php

use PHPUnit\Framework\TestCase as TestCaseAlias;
use Vesmirno\Game\Game;
use Vesmirno\Game\Objects\Buildable\Buildings\Factory;
use Vesmirno\Game\Objects\Growers\Plants\Trees\Deciduous\Fruit\Apple;
use Vesmirno\Game\Objects\Manufacturable\Tools\Axe;
use Vesmirno\Game\Staff\Jobs\Woodcutter;
use Vesmirno\Game\Staff\People\Person;
use Vesmirno\Helper\Config\GameConfigHelper;
use Vesmirno\Interfaces\PlaceForJobInterface;

class JobTest extends TestCaseAlias
{
    /** @var PlaceForJobInterface */
    protected PlaceForJobInterface $place;

    /** @var Game */
    protected Game $game;

    public function setUp(): void
    {
        parent::setUp();

        $this->game = new Game();
        $this->place = new Factory();

        $defaultConfig = file_get_contents(dirname(__DIR__) . '/default.game.config.json');
        $config = new GameConfigHelper($defaultConfig);
        $this->game->start($config);
    }

    public function testCompetences()
    {
        $woodcutterJob = new Woodcutter($this->place, new Axe());
        var_dump($woodcutterJob->competences());
    }

    public function testAssignJobToPerson()
    {
        $tree = new Apple();
        $person = new Person('Ferko Mrkvička');
        $woodcutterJob = new Woodcutter($this->place, new Axe());
        $woodcutterJob->assignTo($person);
        $person->cut($tree);
    }
}