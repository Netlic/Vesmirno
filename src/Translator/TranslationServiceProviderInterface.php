<?php

namespace Vesmirno\Translator;

interface TranslationServiceProviderInterface
{
    /**
     * @param string $value
     * @param string $languageCode
     * @return string|null
     */
    public function provideTranslation(string $value, string $languageCode): ?string;
}