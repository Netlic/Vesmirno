<?php

namespace Vesmirno\Translator;

use Vesmirno\Game\Game;

class LocalTranslation implements TranslationServiceProviderInterface
{
    /** @var string */
    protected $sourcesPath;

    /** @var array */
    protected $translationsArray = [];

    public function __construct(string $sources)
    {
        $this->sourcesPath = Game::system()->translatePath($sources);
    }

    /**
     * @inheritDoc
     */
    public function provideTranslation(string $value, string $languageCode): ?string
    {
        $path = sprintf("%s/%s", $this->sourcesPath, $languageCode);
        $this->loadDirFiles($path);

        return $this->translationsArray[$value] ?? null;
    }

    /**
     * @param string $path
     */
    protected function loadDirFiles(string $path): void
    {
        //TODO: check for JSON errors
        $instance = &$this;
        Game::system()->iterateFilesInDirectory($path, function ($file) use ($instance) {
            $this->translationsArray = array_merge($this->translationsArray, json_decode(file_get_contents($file), true));
        });
    }
}