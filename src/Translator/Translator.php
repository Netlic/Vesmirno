<?php

namespace Vesmirno\Translator;

class Translator
{
    /** @var TranslationServiceProviderInterface[] */
    protected $translationServiceProviders;

    /** @var string */
    protected $activeLanguage;

    public function __construct(array $translationServiceProviders, $activeLanguage)
    {
        $this->translationServiceProviders = $translationServiceProviders;
        $this->activeLanguage = $activeLanguage;
    }

    /**
     * @param TranslationServiceProviderInterface $translationServiceProvider
     * @param string $key
     * @return Translator
     */
    public function addTranslationServiceProvider(TranslationServiceProviderInterface $translationServiceProvider, string $key): self
    {
        $this->translationServiceProviders[$key] = $translationServiceProvider;

        return $this;
    }

    /**
     * Translates given text
     * @param string $value
     * @param string $translationProvider
     * @param string $language
     * @return string
     */
    public function __(string $value, string $translationProvider = null, string $language = null): string
    {
        $language = $language ?? $this->activeLanguage;
        if (!empty($translationProvider) && $this->translationServiceProviders[$translationProvider]) {
            return $this->translationServiceProviders[$translationProvider]->provideTranslation($value, $language);
        }

        /** @var TranslationServiceProviderInterface $translationServiceProvider */
        foreach ($this->translationServiceProviders as $translationServiceProvider) {
            $providedTranslation = $translationServiceProvider->provideTranslation($value, $language);
            if (!empty($providedTranslation)) {
                return $providedTranslation;
            }
        }

        return $value;
    }

    /**
     * @return array
     */
    public function getTranslationProvidersList(): array
    {
        return array_keys($this->translationServiceProviders);
    }
}