<?php

namespace Vesmirno\Interfaces;

interface PlaceForJobInterface
{
    /**
     * @return string
     */
    public function getName() :string;
}