<?php

namespace Vesmirno\Interfaces\Tools;

interface ToolInterface
{
    public function durability() :float;
    public function efficiency() :float;
    public function designedForTraits();
}