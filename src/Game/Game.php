<?php

namespace Vesmirno\Game;

use OutputFormat\Outputter;
use Vesmirno\Commands\CompositeCommand;
use Vesmirno\Helper\Config\ConfigInterface;
use Vesmirno\System\System;
use Vesmirno\Traits\CompositeTrait;
use Vesmirno\Translator\Translator;
use Vesmirno\Verbose\Verbose;

class Game
{
    use CompositeTrait;

    /** @var string */
    private string $stdIn;

    /** @var Verbose|null */
    protected ?Verbose $verbose = null;

    /** @var CompositeCommand */
    protected CompositeCommand $command;

    /** @var ConfigInterface */
    protected ConfigInterface $config;

    /** @var Translator|null */
    protected ?Translator $translator = null;

    /** @var Outputter|null */
    protected ?Outputter $outputter = null;

    /** @var System|null */
    protected ?System $system = null;

    /** @var GameObjectRegister */
    protected GameObjectRegister $gameObjectRegister;

    /** @var Game */
    protected static Game $gameInstance;

    /**
     * @param ConfigInterface $config
     * @param string|null $scenario
     * @throws \Exception
     */
    public function start(ConfigInterface $config, string $scenario = null)
    {
        self::$gameInstance = $this;
        $this->config = $config;
        $this->loadSystem();
        $this->loadTranslations();
        $this->loadVerbose();
        $this->loadCommands();
        $this->loadGameObjectRegister();
        $this->outputter = Outputter::init();
    }

    protected function loadGameObjectRegister()
    {
        $this->gameObjectRegister = new GameObjectRegister();
    }

    protected function loadSystem()
    {
        $this->instantiateData(
            $this->config->get('system-data.main-class'),
            $this->config->get('system-data.other-data-classes'),
            $this->system
        );
    }

    protected function loadTranslations()
    {
        $translationsProviderArray = [];
        foreach ($this->config->get('translations.providers') as $translatorProviderSetting) {
            $translatorProviderClass = $translatorProviderSetting['class'];
            $translationsProviderArray[$translatorProviderSetting['key']] = new $translatorProviderClass(...array_values($translatorProviderSetting['attributes']));
        }
        $this->translator = new Translator($translationsProviderArray, $this->config->get('translations.active-language'));
    }

    protected function loadCommands()
    {
        $commandClass = $this->config->get('command.class');
        $this->command = new $commandClass();
    }

    protected function loadVerbose()
    {
        $this->instantiateData(
            $this->config->get("verbose.main-verbose"),
            $this->config->get("verbose.composite-verbose"),
            $this->verbose);
    }

    public function awaitCommand()
    {
        $stdIn = fopen('php://stdin', 'r');
        $this->writeVerbose($this->verbose->askForCommand());
        $this->processCommand(trim(fgets($stdIn)));
        fclose($stdIn);
    }

    /**
     * @param $command
     */
    protected function processCommand($command)
    {
        $this->command->execute($command);
        $this->writeVerbose($this->verbose->getVerboseBuffer()->emptyBuffer() . PHP_EOL);
        $this->awaitCommand();
    }

    /**
     * @param $string
     * @return boolean
     */
    protected function writeVerbose($string) :bool
    {
        return $this->outputter->printLine($string);
    }

    /**
     * @return System
     */
    public static function system() :System
    {
        return self::$gameInstance->system;
    }

    /**
     * @return ConfigInterface
     */
    public static function config() :ConfigInterface
    {
        return self::$gameInstance->config;
    }

    /**
     * @return Translator
     */
    public static function translator() :Translator
    {
        return self::$gameInstance->translator;
    }

    /**
     * @return Verbose
     */
    public static function verbose() :Verbose
    {
        return self::$gameInstance->verbose;
    }

    /**
     * @return GameObjectRegister
     */
    public static function gameObjectRegister() :GameObjectRegister
    {
        return self::$gameInstance->gameObjectRegister;
    }

    /**
     * @return Outputter
     */
    public static function outputFormatter() :Outputter
    {
        return self::$gameInstance->outputter;
    }
}