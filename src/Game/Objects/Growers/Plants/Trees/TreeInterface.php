<?php

namespace Vesmirno\Game\Objects\Growers\Plants\Trees;

interface TreeInterface
{
    public function getWoodQuality();
    public function getHeight();
    public function getWeight();
}