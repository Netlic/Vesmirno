<?php

namespace Vesmirno\Game\Objects\Growers\Plants\Trees;

use Vesmirno\Game\Objects\Growers\Grower;
use Vesmirno\Game\Objects\Resources\Wood\Wood;
use Vesmirno\Interfaces\Material\CutableInterface;

abstract class Tree extends Grower implements TreeInterface, CutableInterface
{
    /**
     * @inheritDoc
     */
    public function resourceAfterHarvest(): array
    {
        return [Wood::harvestWood($this)];
    }
}