<?php

namespace Vesmirno\Game\Objects\Growers\Plants\Trees\Deciduous\Fruit;

use Vesmirno\Game\Objects\Growers\GrowsFruitInterface;
use Vesmirno\Game\Objects\Growers\Plants\Trees\Tree;

abstract class FruitTree extends Tree implements GrowsFruitInterface
{
    /**
     * @inheritDoc
     */
    public function resourceAfterHarvest(): array
    {
        return [$this->getFruit()] + parent::resourceAfterHarvest();
    }
}