<?php

namespace Vesmirno\Game\Objects\Growers;

interface GrowsFruitInterface
{
    public function getFruit();
}