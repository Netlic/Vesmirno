<?php

namespace Vesmirno\Game\Objects\Growers;

use Vesmirno\Game\GameObject;
use Vesmirno\Game\Objects\Resources\AbleToBeHarvested;
use Vesmirno\Traits\Metrics\AgeTrait;
use Vesmirno\Traits\Metrics\HeightTrait;
use Vesmirno\Traits\Metrics\WeightTrait;

abstract class Grower extends GameObject implements AbleToBeHarvested
{
    use HeightTrait, WeightTrait, AgeTrait;

    public function grow(float $height, float $weight)
    {
        $this->setHeight($height);
        $this->setWeight($weight);
    }
}