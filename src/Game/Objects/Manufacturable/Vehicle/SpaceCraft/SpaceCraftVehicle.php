<?php

namespace Vermirno\Game\Objects\Manufacturable\Vehicle\SpaceCraft;

use Vermirno\Game\Objects\Environment\AirEnvironment;
use Vermirno\Game\Objects\Environment\SpaceEnvironment;
use Vermirno\Game\Objects\Manufacturable\Engine\SpaceCraftEngine;
use Vermirno\Game\Objects\Manufacturable\Hull\SpaceCraftHull;
use Vermirno\Game\Objects\Manufacturable\Hull\VehicleHull;
use Vermirno\Game\Objects\Manufacturable\Vehicle\Vehicle;
use Vermirno\Game\Objects\Interfaces\EngineInterface;

class SpaceCraftVehicle extends Vehicle
{

    /**
     * Creates new spacecraft vehicle
     * @param EngineInterface $engine
     * @param VehicleHull $hull
     */
    public function __construct(
        SpaceCraftEngine $engine,
        SpaceCraftHull $hull
    )
    {
        parent::__construct($engine, $hull);
       
        var_dump('New spacecraft available!');
    }

    /*public function getShield()
    {
        return $this->shiled;
    }*/

    protected function vehicleType()
    {
        return 'spacecraft';
    }

    public static function declareItemid()
    {
        return 'Spacecraft vehicle';
    }

    public static function defineEnvironmentSchema()
    {
        return [
            'allowedEnvironments' => [
                SpaceEnvironment::class,
                AirEnvironment::class
            ]
        ];
    }

}
