<?php

namespace Vesmirno\Game\Objects\Manufacturable\Vehicle;

interface VehicleInterface
{
    public function getEngine();
    public function getHull();
}
