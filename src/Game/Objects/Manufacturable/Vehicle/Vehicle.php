<?php

namespace Vermirno\Game\Objects\Manufacturable\Vehicle;

use Vermirno\Game\Objects\Manufacturable\Hull\VehicleHull;
use Vermirno\Game\Objects\Interfaces\EngineInterface;
use Vermirno\Game\Objects\Interfaces\EnvironmentInterface;
use Vermirno\Game\Objects\Interfaces\ManufacturableInterface;
use Vermirno\Game\Objects\Interfaces\VehicleInterface;
use Vermirno\Game\Objects\Traits\ItemTrait;

abstract class Vehicle implements VehicleInterface, ManufacturableInterface
{
    const ENVIRO_ALLOWED = 'allowedEnvironments';

    use ItemTrait;

    /** @var EngineInterface */
    protected $engine;

    /** @var VehicleHull */
    protected $hull;

    /** @var EnvironmentInterface[] */
    protected $availableFoEnvironment = [];

    public function __construct(
        EngineInterface $engine,
        VehicleHull $hull)
    {
        $this->engine = $engine;
        $this->hull = $hull;
        $this->availableFoEnvironment = $this->createEnvironments();
    }

    /**
     * 
     * @return EngineInterface
     */
    public function getEngine()
    {
        return $this->engine;
    }

    /**
     * 
     * @return HullInterface
     */
    public function getHull()
    {
        return $this->hull;
    }

    /**
     * 
     * @return EnvironmentInterface[]
     */
    protected function createEnvironments()
    {
        $enviros = static::defineEnvironmentSchema();
        $enviroInstances = [];
        foreach ($enviros[self::ENVIRO_ALLOWED] as $enviro) {
            if (class_exists($enviro)) {
                $enviroInstances[$enviro] = new $enviro();
            }
        }
        
        return $enviroInstances;
    }

    /**
     * 
     * @param EnvironmentInterface $foreignEnvironment
     * @return boolean
     */
    /*public function isSuitedForEnvironment(EnvironmentInterface $foreignEnvironment)
    {
        $match = $foreignEnvironment->getDescription() === $this->environment()->getDescription();

        return $match;
    }*/
    
    
    /**
     * 
     * @return EnvironmentInterface
     */
    /*protected function environment()
    {
        return $this->environmentSuitedFor();
    }*/
    
    protected abstract function vehicleType();

}
