<?php

namespace Vesmirno\Game\Objects\Manufacturable;

/**
 *
 * @author brano
 */
interface ManufacturableInterface
{
    public static function declareItemId();
    public static function defineEnvironmentSchema();
}
