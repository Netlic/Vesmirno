<?php

namespace Vesmirno\Game\Objects\Manufacturable;

interface EngineInterface
{
    public function getPowerOutput();
    public function getFuelType();
}
