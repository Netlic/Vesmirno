<?php

namespace Vermirno\Game\Objects\Manufacturable\Engine;

use Vesmirno\Game\Objects\Manufacturable\EngineInterface;
use Vesmirno\Game\Objects\Manufacturable\ManufacturableInterface;

abstract class VehicleEngine implements EngineInterface, ManufacturableInterface
{
    //put your code here
}
