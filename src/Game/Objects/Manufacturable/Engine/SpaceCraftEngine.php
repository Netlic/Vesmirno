<?php

namespace Vermirno\Game\Objects\Manufacturable\Engine;

use Vermirno\Game\Objects\Vehicle\SpaceCraft\SpaceCraftVehicle;

/**
 * Description of SpaceCraftEngine
 *
 * @author brano
 */
class SpaceCraftEngine extends VehicleEngine
{
    public function getPowerOutput()
    {
        
    }

    public static function declareItemid()
    {
        return 'Spacecraft engine';
    }

    public static function defineEnvironmentSchema()
    {
        return SpaceCraftVehicle::defineEnvironmentSchema();
    }

    public function getFuelType()
    {
        // TODO: Implement getFuelType() method.
    }
}
