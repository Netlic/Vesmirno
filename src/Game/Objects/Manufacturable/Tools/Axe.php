<?php

namespace Vesmirno\Game\Objects\Manufacturable\Tools;

use Vesmirno\Interfaces\Tools\CutInterface;

class Axe extends Tool implements CutInterface
{

    public function designedForTraits()
    {
        return [
            'hand', 'strength'
        ];
    }
}