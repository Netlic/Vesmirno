<?php

namespace Vesmirno\Game\Objects\Manufacturable\Tools;

use Vesmirno\Game\GameObject;
use Vesmirno\Interfaces\Tools\ToolInterface;
use Vesmirno\Traits\Metrics\AgeTrait;
use Vesmirno\Traits\Metrics\HeightTrait;
use Vesmirno\Traits\Metrics\WeightTrait;

abstract class Tool extends GameObject implements ToolInterface
{
    use WeightTrait, HeightTrait, AgeTrait;
    //TODO: get efficiency begin index from config
    //TODO: determine requirement traits for using the tool
    public function efficiency() :float
    {
        return 1;
    }

    public function durability() :float
    {
        return 1;
    }

    protected function determineToolRequirements()
    {
        $this->designedForTraits();
    }
}