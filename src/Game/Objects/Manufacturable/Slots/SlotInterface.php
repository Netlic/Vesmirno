<?php

namespace Vesmirno\Game\Objects\Manufacturable\Slots;

use Vesmirno\Game\Objects\Manufacturable\SlotItem\SlotItemInterface;

interface SlotInterface
{
    public function addItem(SlotItemInterface $item);
    public function removeItem();
    public function isEquipped();
    public function inspectItem();
    public function getMaximumItemsCount();
}
