<?php

namespace Vesmirno\Game\Objects\Manufacturable\Slots;

use Vesmirno\Game\Objects\Manufacturable\SlotItem\SlotItemInterface;

abstract class ItemSlot implements SlotInterface
{

    /** @var SlotItemInterface */
    protected $item;

    /**
     * 
     * @param SlotItemInterface $item
     * @return $this
     */
    public function addItem(SlotItemInterface $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * 
     * @return SlotItemInterface
     */
    public function inspectItem()
    {
        return $this->item;
    }

    /**
     * 
     * @return boolean
     */
    public function isEquipped()
    {
        return (bool) $this->item;
    }

    /**
     * 
     */
    public function removeItem()
    {
        $this->item = null;
    }

}
