<?php

namespace Vesmirno\Game\Objects\Manufacturable\Slots\VehicleSlot;

use Vermirno\Game\Objects\Manufacturable\SlotItem\Weapon;
use Vesmirno\Game\Objects\Manufacturable\SlotItem\SlotItemInterface;
use Vesmirno\Game\Objects\Manufacturable\Slots\ItemSlot;

/**
 * Description of WeaponSlot
 *
 * @author brano
 */
class WeaponSlot extends ItemSlot
{

    /**
     * 
     * @return SlotItemInterface
     */
    public function inspectItem() : Weapon
    {
        return $this->weapon;
    }

    public function getMaximumItemsCount()
    {
        // TODO: Implement getMaximumItemsCount() method.
    }
}
