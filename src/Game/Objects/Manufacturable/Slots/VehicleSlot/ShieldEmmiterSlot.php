<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Vesmirno\Game\Objects\Manufacturable\Slots\VehicleSlot;


use Vermirno\Game\Objects\Manufacturable\SlotItem\Shield;
use Vesmirno\Game\Objects\Manufacturable\SlotItem\SlotItemInterface;
use Vesmirno\Game\Objects\Manufacturable\Slots\ItemSlot;

/**
 * Description of ShieldEmmiterSlot
 *
 * @author brano
 */
class ShieldEmitterSlot extends ItemSlot
{
    /**
     * 
     * @return SlotItemInterface
     */
    public function inspectItem() : Shield
    {
        return parent::inspectItem();
    }

    public function getMaximumItemsCount()
    {
        // TODO: Implement getMaximumItemsCount() method.
    }
}
