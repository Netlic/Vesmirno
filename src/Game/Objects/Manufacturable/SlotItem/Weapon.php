<?php

namespace Vermirno\Game\Objects\Manufacturable\SlotItem;

use Vesmirno\Game\Objects\Manufacturable\ManufacturableInterface;
use Vesmirno\Game\Objects\Manufacturable\SlotItem\SlotItemInterface;

/**
 * Description of Weapon
 *
 * @author brano
 */
class Weapon implements SlotItemInterface, ManufacturableInterface
{
    /** @var string */
    private $name;

    /** @var int */
    private $damage;

    /** @var string */
    private $damageType;

    public function __construct(string $name, int $damage, string $damageType)
    {
        $this->name = $name;
        $this->damage = $damage;
        $this->damageType = $damageType;
    }

    public function __toString()
    {
        return $this->name;
    }
    
    public function fire()
    {
        return $this->damage;
    }

    public static function declareItemid()
    {
        return 'Weapon';
    }

    public static function defineEnvironmentSchema()
    {
        
    }

}
