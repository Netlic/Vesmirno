<?php

namespace Vermirno\Game\Objects\Manufacturable\SlotItem;

use Vesmirno\Game\Objects\Manufacturable\ManufacturableInterface;
use Vesmirno\Game\Objects\Manufacturable\SlotItem\SlotItemInterface;

class Shield implements SlotItemInterface, ManufacturableInterface
{
    /** @var string */
    private $damageTypeAbsorb;
    
    /** @var int */
    private $strength;

    public function __construct(int $strength, string $damageTypeAbsorb)
    {
        $this->strength = $strength;
        $this->damageTypeAbsorb = $damageTypeAbsorb;
    }

    /**
     * 
     * @return int
     */
    public function getStrength()
    {
        return $this->strength;
    }
    
    /**
     * 
     * @param string $damageType
     * @return boolean
     */
    public function isShieldApplicable(string $damageType) : bool
    {
        return $this->damageTypeAbsorb === $damageType;
    }

    /**
     * 
     * @param int $amount
     * @return int
     */
    public function decreaseStrength(int $amount) : int
    {
        $this->strength -= $amount;
        
        return $this->strength;
    }

    /**
     * 
     * @return boolean
     */
    public function isDepleted() : bool
    {
        return $this->strength <= 0;
    }

    public static function declareItemid()
    {
        return 'Shield';
    }

    public static function defineEnvironmentSchema()
    {
        
    }

}
