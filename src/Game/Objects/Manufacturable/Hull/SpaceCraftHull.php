<?php

namespace Vermirno\Game\Objects\Manufacturable\Hull;

use Vermirno\Game\Objects\Manufacturable\Vehicle\SpaceCraft\SpaceCraftVehicle;

class SpaceCraftHull extends VehicleHull
{
    public static function declareItemId()
    {
        return 'Spacecraft hull';
    }

    public static function defineEnvironmentSchema()
    {
        return SpaceCraftVehicle::defineEnvironmentSchema();
    }
}