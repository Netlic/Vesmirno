<?php

namespace Vesmirno\Game\Objects\Resources\Wood;

use Vesmirno\Game\Objects\Growers\Plants\Trees\TreeInterface;
use Vesmirno\Game\Objects\Resources\ResourceInterface;

abstract class Wood implements ResourceInterface
{
    public static function harvestWood(TreeInterface $tree)
    {
        var_dump(self::class, get_class($tree));
    }
}