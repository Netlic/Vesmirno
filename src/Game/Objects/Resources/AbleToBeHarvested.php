<?php

namespace Vesmirno\Game\Objects\Resources;

interface AbleToBeHarvested
{
    /**
     * @return ResourceInterface[]
     */
    public function resourceAfterHarvest(): array;
}