<?php

namespace Vesmirno\Material;

use Vesmirno\Material\MaterialComponent\MaterialComponent;

class Material
{

    /** @var MaterialComponent[] */
    private $components = [];

    public function __construct(array $components)
    {
        $this->components = $components;
    }

    /**
     * @return int
     */
    public function getMaterialStrength(): int
    {
        $totalStrength = 0;
        foreach ($this->components as $component) {
            $totalStrength += $component->getStrength();
        }

        return ceil($totalStrength / count($this->components));
    }

}
