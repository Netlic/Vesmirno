<?php

namespace Vesmirno\Material\MaterialComponent;

class MaterialComponent
{
    /** @var string */
    private $name;

    /** @var int */
    private $strength;
    
    public function __construct(string $name, int $strength)
    {
        $this->name = $name;
        $this->strength = $strength;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
