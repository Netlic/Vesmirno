<?php

namespace Vesmirno\Game\Objects\Industrial;

use ReflectionClass;

use ReflectionException;
use Vesmirno\Game\Objects\Blueprints\ModelInterface;
use Vesmirno\Game\Objects\Manufacturable\ManufacturableInterface;

class AssemblyLine
{

    /** @var string */
    private $productClass;

    /** @var string */
    private $assemblyLineName;

    /** @var array */
    private $dependentSupplies = [];

    /** @var array */
    private $unusedSupplies;
    
    /** @var ModelInterface */
    private $model;

    /** @var array */
    private static $assembledProducts = [];

    public function __construct(string $name, ModelInterface $model = null)
    {
        $this->setModelForProduction($model);
        $this->assemblyLineName = $name;
    }

    /**
     * To easily set model for production
     * @param ModelInterface|null $model
     * @return AssemblyLine
     */
    public function setModelForProduction(ModelInterface $model = null) : self
    {
        if (!empty($model) && $this->isManufacturable($model->isBlueprintFor())) {
            $this->model = $model;
            $this->productClass = $this->model->isBlueprintFor();
        }
        
        return $this;
    }

    /**
     * Parts being produced here
     * @return array
     */
    public static function getAssembledProducts()
    {
        return static::$assembledProducts;
    }

    /**
     * See what parts assembly line demands
     * @return ManufacturableInterface[]
     * @throws ReflectionException
     */
    public function getDependentSupplies()
    {
        if (!$this->dependentSupplies) {
            $reflection = new ReflectionClass($this->productClass);
            $constructor = $reflection->getConstructor();
            if ($constructor) {
                foreach ($constructor->getParameters() as $parameter) {
                    $paramType = (string) $parameter->getType();
                    if (class_exists($paramType) && $this->isManufacturable($paramType)) {
                        $this->dependentSupplies[] = $paramType;
                    }
                }
            }
        }

        return $this->dependentSupplies;
    }

    /**
     * Assemble particular product
     * @param ManufacturableInterface[] $supplies
     * @return ManufacturableInterface|null
     * @throws ReflectionException
     */
    public function assembleProduct(array $supplies = [])
    {
        $neededSupplies = $this->getDependentSupplies();
        $providedSupplies = $supplies;

        $diff = array_diff($neededSupplies, array_keys($providedSupplies));


        foreach ($diff as $notSupplied) {
            $missingTypeId = call_user_func([$notSupplied, 'declareItemid']);
            $currentProductId = call_user_func([$this->productClass, 'declareItemid']);
            var_dump("We don't have this part -- '$missingTypeId' -- to finish the product ** '$currentProductId' ** .");
        }

        if (empty($diff)) {
            $product = $this->productClass;

            return new $product(...$providedSupplies);
        }

        $this->unusedSupplies = $providedSupplies;

        return null;
    }

    /**
     * Can we produce given part?
     * @param string $product
     * @return boolean
     */
    public function isManufacturable(string $product)
    {
        $implementations = class_implements($product);

        return $implementations !== false && in_array(ManufacturableInterface::class, $implementations);
    }

    /**
     * Return unused supplies if the production is not successfull, if not returned to stock, then destroyed!
     * @return ManufacturableInterface[]|null
     */
    public function getUnusedSupplies()
    {
        $unusedSupplies = $this->unusedSupplies;
        $this->unusedSupplies = null;

        return $unusedSupplies;
    }

}
