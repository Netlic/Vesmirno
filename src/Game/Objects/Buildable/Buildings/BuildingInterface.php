<?php

namespace Vesmirno\Game\Objects\Buildable\Buildings;

interface BuildingInterface
{
    //public function getBuildingManagement();
    public function getFloors();
}
