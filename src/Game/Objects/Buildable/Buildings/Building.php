<?php

namespace Vesmirno\Game\Objects\Buildable\Buildings;

//use Vesmirno\Game\Objects\Buildings\BuildingInterface;
//use Vesmirno\Interfaces\BuildingServiceInterface;

//TODO - building naming

use Vesmirno\Interfaces\PlaceForJobInterface;

abstract class Building implements BuildingInterface, PlaceForJobInterface
{

    /** @var BuildingServiceInterface */
    protected $buildingManagement;

//    public function __construct(BuildingServiceInterface $buildingManagement)
//    {
//        $this->buildingManagement = $buildingManagement;
//        $this->buildingManagement->assignToBuilding($this);
//    }

    /**
     * Get building management
     * @return BuildingServiceInterface
     */
    /*public function getBuildingManagement(): BuildingServiceInterface
    {
        return $this->buildingManagement;
    }*/
    
    /*public function __call($name, $arguments)
    {
        return call_user_func_array([$this->buildingManagement, $name], $arguments);
    }*/
}
