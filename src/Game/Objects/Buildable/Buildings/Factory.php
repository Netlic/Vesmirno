<?php

namespace Vesmirno\Game\Objects\Buildable\Buildings;

use Vesmirno\Building\BuildingServices\FactoryManagement;
use Vesmirno\Game\Objects\Blueprints\ModelInterface;
use Vesmirno\Game\Objects\Industrial\AssemblyLine;
use Vesmirno\Game\Objects\Manufacturable\ManufacturableInterface;
use Vesmirno\Game\Staff\Jobs\FactoryJobs\StorageLogisticsManager;


/**
 * Factory building, can produce anything via its assembly lines
 *
 * @method FactoryManagement addAssemblyLine(ModelInterface $model, string $lineName = null) Adds assembly line to assembly lines array
 * @method [] getAssemblyLineNames() Gets assembly lines names
 * @method AssemblyLine getAssemblyLineByName(string $name) 
 * @method AssemblyLine[] getAssemblyLines() Gets assembly line array
 * @method FactoryManagement setAssemblyLineProductionPriorityWeight(int $priority, string $name = null) Sets assembly line produciton priority
 * @method FactoryManagement renameAssemblyLine(string $originalName, string $newName) Renames assembly line
 * @method [] getAssemblyLinePriorities() Gets assembly lines priority array
 * @method FactoryManagement addToStock(ManufacturableInterface $product)
 * @method ManufacturableInterface removeFromStock(string $product)
 * @method array askAssemblyLineSupplies(AssemblyLine $line, StorageLogisticsManager $storekeeper) Production manager asks for supplies for assembly line from storage manager
 * @method array storagePreview() Building storage preview
 * @method array getAssemblyLineOutputInfo(mixed $assemblyLine)
 */

class Factory extends Building
{

    /** @var FactorableInterface[] */
    protected $storage = [];

    /**
     * Creates new factory building instance :)
     * @param FactoryManagement $factoryManagement
     * @param ModelInterface $model
     * @param string|null $lineName
     */
    /*public function __construct(FactoryManagement $factoryManagement, ModelInterface $model = null, string $lineName = null)
    {
        parent::__construct($factoryManagement);
        if ($model) {
            $this->addAssemblyLine($model, $lineName);
        }
    }*/

    /**
     * Produces products on all assembly lines by given priority
     * @throws \ReflectionException
     */
    /*public function produce()
    {
        /** @var StorageLogisticsManager $storekeeper */
        /*$storekeeper = $this->buildingManagement->getManager(StorageLogisticsManager::class);
        if ($storekeeper) {
            foreach ($this->getAssemblyLinePriorities() as $line) {
                $lineObj = $this->getAssemblyLineByName($line);
                $supplies = $this->askAssemblyLineSupplies($lineObj, $storekeeper);
                $this->addToStock($lineObj->assembleProduct($supplies));

                //Return supplied parts to stock after unsuccessfull attempt to create product
                $unusedSupplies = $lineObj->getUnusedSupplies();

                if ($unusedSupplies) {
                    $storekeeper->returnToStock($unusedSupplies);
                }
            }
        }
        //else throw error
    }*/

    public function getFloors()
    {
        // TODO: Implement getFloors() method.
    }

    public function getName(): string
    {
        return '';
    }
}
