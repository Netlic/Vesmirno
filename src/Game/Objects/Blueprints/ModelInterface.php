<?php

namespace Vesmirno\Game\Objects\Blueprints;

interface ModelInterface
{

    /**
     * Gets model requirements
     * @return array Array of model requirements
     */
    public static function modelRequirements() : array;

    /**
     * Additional calculations related to model property
     * @return array Methods containing calculations
     */
    public static function additionalCalculations() : array;

    /**
     * Determines for which final product is model being created
     * @return string Class of factorable item
     */
    public function isBlueprintFor() : string;

}
