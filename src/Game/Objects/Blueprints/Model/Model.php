<?php

namespace Vesmirno\Game\Objects\Blueprints\Model;

use Vesmirno\Game\Objects\Blueprints\ModelInterface;
use Vesmirno\Material\Material;

/**
 * @property float $height
 * @property float $width
 * @property float $depth
 * @property array $slots
 * @property string $modelName
 * @property Material $material
 */
abstract class Model implements ModelInterface
{

    /** @var array */
    protected $modelProps = [];
    
    public function __construct(array $props = [])
    {
        $this->modelProps = $props;
    }

    public function __get($name)
    {
        return $this->modelProps[$name] ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public static function modelRequirements() : array
    {
        return [
            'height' => 'float',
            'width' => 'float',
            'depth' => 'float',
            'modelName' => 'string',
            'material' => Material::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function additionalCalculations() : array
    {
        return [];
    }

}
