<?php

namespace Vesmirno\Model\Engine;


use Vermirno\Game\Objects\Manufacturable\Engine\SpaceCraftEngine;
use Vesmirno\Model\Model;

class EngineModel extends Model
{
    /**
     * @inheritDoc
     */
    public function isBlueprintFor(): string
    {
        return SpaceCraftEngine::class;
    }
}
