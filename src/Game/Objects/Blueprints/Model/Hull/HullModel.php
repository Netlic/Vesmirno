<?php

namespace Vesmirno\Game\Objects\Blueprints\Model\Hull;


use Vermirno\Game\Objects\Manufacturable\Hull\SpaceCraftHull;
use Vesmirno\Game\Objects\Blueprints\Model\Model;
use Vesmirno\Game\Objects\Manufacturable\Slots\SlotInterface;

/**
 * @property SlotInterface[] $slots
 */
class HullModel extends Model
{

    /**
     * {@inheritdoc}
     */
    public static function modelRequirements() : array
    {
        return ['slots' => SlotInterface::class . '[]'] + parent::modelRequirements();
    }

    /**
     * {@inheritdoc}
     */
    public static function additionalCalculations() : array
    {
        return [
            'slots' => [
                'calculateHeight',
                'calculateWidth'
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function isBlueprintFor(): string
    {
        return SpaceCraftHull::class;
    }
}
