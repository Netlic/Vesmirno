<?php

namespace Vesmirno\Game\Staff\Jobs;

use Vesmirno\Game\Staff\People\PersonInterface;

interface JobInterface
{
    /**
     * @return array
     */
    public function competences(): array;

    /**
     * @return mixed
     */
    public function requirements();

    /**
     * @return string
     */
    public function jobDescription(): string;

    public function at();

    /**
     * @param PersonInterface $person
     * @return mixed
     */
    public function assignTo(PersonInterface $person);

    /**
     * @param string $competence
     * @return bool
     */
    public function includesCompetence(string $competence): bool;
}
