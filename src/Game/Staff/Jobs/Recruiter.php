<?php

namespace Vesmirno\Game\Staff\Jobs;

use Vesmirno\Traits\Job\RecruiterTrait;

class Recruiter extends Job
{
    use RecruiterTrait;

    public function requirements()
    {
        // TODO: Implement requirements() method.
    }

    public function jobDescription(): string
    {
        // TODO: Implement jobDescription() method.
    }

    public function competences() :array
    {
        return ['findPersonForJob'];
    }
}