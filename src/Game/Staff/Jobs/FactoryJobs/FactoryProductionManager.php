<?php

namespace Vesmirno\Game\Staff\Jobs\FactoryJobs;

use Vesmirno\Game\Objects\Industrial\AssemblyLine;
use Vesmirno\Game\Staff\Jobs\Job;

class FactoryProductionManager extends Job
{

    private $assemblyLines = [];
    private $assemblyLineProductionPriority = [];

    /**
     * Adds assembly line to assembly lines array
     * @param string $product
     * @param string $lineName
     */
    public function addAssemblyLine(string $product, string $lineName = null)
    {
        if (!$lineName) {
            $lineName = $this->assignLineName();
        }

        $this->assemblyLineProductionPriority[] = $lineName;
        $this->assemblyLines[$lineName] = new AssemblyLine($lineName, $product);
    }

    /**
     * Gets assembly lines names
     * @return array
     */
    public function getAssemblyLineNames(): array
    {
        return array_keys($this->assemblyLines);
    }

    /**
     * 
     * @param string $name
     * @return AssemblyLine|null
     */
    public function getAssemblyLineByName(string $name)
    {
        return $this->assemblyLines[$name] ?? null;
    }

    /**
     * Gets assembly line array
     * @return array
     */
    public function getAssemblyLines(): array
    {
        return $this->assemblyLines;
    }

    /**
     * Sets assembly line produciton priority
     * @param int $priority
     * @param string $name
     * @return $this
     */
    public function setAssemblyLineProductionPriorityWeight(int $priority, string $name = null)
    {
        if (!$name) {
            $lineNames = array_keys($this->assemblyLines);
            $name = end($lineNames);
        }

        $key = array_search($name, $this->assemblyLineProductionPriority);
        $this->changePriorityChain($priority, $key);
        $this->assemblyLineProductionPriority[$priority] = $name;

        if ($this->assemblyLineProductionPriority[$key] == $name) {
            unset($this->assemblyLineProductionPriority[$key]);
        }

        return $this;
    }

    /**
     * Renames assembly line
     * @param string $originalName
     * @param string $newName
     * @return $this
     */
    public function renameAssemblyLine(string $originalName, string $newName)
    {
        $line = $this->assemblyLines[$originalName] ?? null;
        if ($line && !array_key_exists($newName, array_keys($this->assemblyLines))) {
            unset($this->assemblyLines[$originalName]);
            $this->assemblyLines[$newName] = $line;
            $priority = array_search($originalName, $this->assemblyLineProductionPriority);
            $this->assemblyLineProductionPriority[$priority] = $newName;
        } elseif (array_key_exists($newName, array_keys($this->assemblyLines))) {
            //error - assembly line with this name already exists
        } else {
            //error - assembly line does not exists
        }

        return $this;
    }

    /**
     * Gets assembly lines priority array
     * @return array
     */
    public function getAssemblyLinePriorities()
    {
        return $this->assemblyLineProductionPriority;
    }

    /**
     * Asks supply parts for assembly lines from storege manager
     * @param AssemblyLine $line
     * @param StorageLogisticsManager $storekeeper
     * @return FactorableInterface[]
     */
    public function askAssemblyLineSupplies(AssemblyLine $line, StorageLogisticsManager $storekeeper)
    {
        $supplies = [];
        foreach ($line->getDependentSupplies() as $supply) {
            if ($storekeeper->isInStock($supply)) {
                $supplies[$supply] = $storekeeper->removeFromStock($supply);
            }
        }

        return $supplies;
    }

    /**
     * Create generic assembly line name
     * @return string
     */
    private function assignLineName()
    {
        $linesCount = count($this->assemblyLines) + 1;

        return 'Line' . $linesCount;
    }

    /**
     * 
     * @param integer $priority
     */
    private function changePriorityChain($priority, $key)
    {
        $line = $this->assemblyLineProductionPriority[$priority] ?? null;
        if ($line) {
            if ($priority !== $key) {
                $this->changePriorityChain($priority + 1, $key);
                $this->assemblyLineProductionPriority[$priority + 1] = $line;
            }
        }
    }

}
