<?php

namespace Vesmirno\Game\Staff\Jobs;

use Vesmirno\Interfaces\PlaceForJobInterface;
use Vesmirno\Interfaces\Tools\CutInterface;
use Vesmirno\Traits\Job\WoodcutterTrait;

class Woodcutter extends Job
{
    use WoodcutterTrait;

    public function __construct(PlaceForJobInterface $place, CutInterface $tool = null)
    {
        if ($tool) {
            $this->tool = $tool;
            $this->toolBox[] = $tool;
        }
        parent::__construct($place);
    }

    /**
     * @inheritDoc
     */
    public function competences(): array
    {
        return ['cut'];
    }

    public function jobDescription(): string
    {
        // TODO: Implement jobDescription() method.
    }
}