<?php

namespace Vesmirno\Game\Staff\Jobs;

use Vesmirno\Game\GameObject;
use Vesmirno\Game\Staff\People\PersonInterface;

final class JobsRegister
{
    /** @var array */
    private $jobRegister;

    /**
     * @param PersonInterface $person
     * @return JobsRegister
     */
    public function register(PersonInterface $person)
    {
        $this->jobRegister[$person->getJob()][$person->getObjectId()] = $person;

        return $this;
    }

    public function unregister(PersonInterface $person)
    {
        unset($this->jobRegister[$person->getJob()][$person->getObjectId()]);

        return $this;
    }

    public function getOnePersonByJob(string $jobName, string $objectId = null)
    {
        if (empty($this->jobRegister[$jobName])) {
            return null;
        }

        if ($objectId) {
            if (empty($this->jobRegister[$jobName][$objectId])) {
                return null;
            }

            return $this->jobRegister[$jobName][$objectId];
        }

        return $this->jobRegister[array_rand($this->jobRegister)];
    }

    public function getManyPersonsByJob(string $jobName, integer $personsCount, array $objectIds = null)
    {

    }
}