<?php

namespace Vesmirno\Game\Staff\Jobs;

use Vesmirno\Game\Staff\People\PersonInterface;
use Vesmirno\Interfaces\PlaceForJobInterface;
use Vesmirno\Traits\Job\JobTrait;

abstract class Job implements JobInterface
{
    use JobTrait;

    /** @var PlaceForJobInterface */
    protected PlaceForJobInterface $place;

    /** @var PersonInterface|null */
    protected ?PersonInterface $person = null;

    /**
     * Job constructor.
     * @param PlaceForJobInterface $place
     * @param PersonInterface|null $person
     */
    public function __construct(PlaceForJobInterface $place, PersonInterface $person = null)
    {
        $this->place = $place;
        if ($person) {
            $this->person = $person;
            $this->person->setJob($this);
        }
    }

    /**
     * @return PlaceForJobInterface
     */
    public function at() :PlaceForJobInterface
    {
        return $this->place;
    }

    public function assignTo(PersonInterface $person)
    {
        $this->person = $person;
        $this->person->setJob($this);
    }

    /**
     * Gets given job competences
     * @return array
     */
    public function competences(): array
    {
        return [];
    }

    public function __toString()
    {
        return $this->jobDescription();
    }

    /**
     * @param string $competence
     * @return bool
     */
    public function includesCompetence(string $competence): bool
    {
        return in_array($competence, $this->competences());
    }

    /**
     * @return array
     */
    public function getProfessionTrait() :array
    {
        return $this->competences();
    }

    public function requirements()
    {
        $this->toolBox;
    }
}
