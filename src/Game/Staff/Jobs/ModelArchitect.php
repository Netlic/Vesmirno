<?php

namespace Vesmirno\Jobs;

use Vesmirno\Exceptions\JobException;
use Vesmirno\Exceptions\ModelArchitectException;
use Vesmirno\Game\Objects\Blueprints\Model\Model;
use Vesmirno\Game\Staff\Jobs\Job;
use Vesmirno\Helper\ModelCalculationHelper;
use Vesmirno\Traits\ModelPropValidatorTrait;

class ModelArchitect extends Job
{

    use ModelPropValidatorTrait;

    /** @var string */
    private $model;

    /** @var array */
    private $propsToSupply = [];

    /** @var array */
    private $suppliedProps = [];

    /**
     * Accepts and examines model requirement schema
     * @param string $modelClass Class of model we want architect to create
     * @return ModelArchitect
     * @throws ModelArchitectException
     */
    public function examineModel(string $modelClass)
    {
        if (!is_subclass_of($modelClass, Model::class)) {
            throw new ModelArchitectException('Not a model.');
        }

        $this->model = $modelClass;
        $this->propsToSupply = call_user_func([$this->model, 'modelRequirements']);

        return $this;
    }

    /**
     * Returns a list of properties that needs to be supplied
     * before model is created
     * @return array
     */
    public function whatAttributesToSupply()
    {
        return array_diff(array_keys($this->propsToSupply), array_keys($this->suppliedProps));
    }

    /**
     * Supplies property required for model to be created
     * @param string $prop Model property to supply
     * @param mixed $value Value of supplied property
     * @return ModelArchitect
     * @throws JobException
     */
    public function supplyProp(string $prop, $value)
    {
        if (!in_array($prop, array_keys($this->propsToSupply))) {
            throw new ModelArchitectException('Non required property');
        }

        $propType = $this->propsToSupply[$prop];
        if (!$this->isPropValueValid($value, str_replace('[]', '', $propType))) {
            throw new ModelArchitectException(sprintf('Invalid value for property: %s', $prop));
        }

        if ($this->isPropArrayOfValues($propType)) {
            $this->suppliedProps[$prop][] = $value;
        } else {
            $this->suppliedProps[$prop] = $value;
        }

        $additionalCalculations = call_user_func([$this->model, 'additionalCalculations']);
        if (array_key_exists($prop, $additionalCalculations)) {
            foreach ($additionalCalculations[$prop] as $calculation) {
                $result = call_user_func_array([ModelCalculationHelper::class, $calculation], $this->suppliedProps[$prop]);
                if (!empty(array_diff(array_keys($result), ['prop', 'value']))) {
                    throw new ModelArchitectException('Unable to calculate additional props');
                }

                $this->supplyProp($result['prop'], $result['value']);
            }
        }

        return $this;
    }

    /**
     * 
     * @param string $prop
     * @return boolean
     */
    public function isPropSupplied(string $prop)
    {
        return (bool) $this->suppliedProps[$prop] ?? false;
    }

    /**
     * Tell architect to finish the model
     * @param string $modelClass
     * @param array $propsWithValues
     * @return ModelInterface|null
     * @throws JobException
     */
    public function completeModel(string $modelClass = null, array $propsWithValues = [])
    {
        if (!$this->model) {
            if (!$modelClass) {
                throw new ModelArchitectException('What model do you wish to complete?');
            }
            $this->examineModel($modelClass);
            if (!empty($propsWithValues)) {
                foreach($propsWithValues as $prop => $value) {
                    $this->supplyProp($prop, $value);
                }
            }
        }
        if (!$this->canModelBeFinished()) {
            $notSupplied = implode(', ', $this->whatAttributesToSupply());
            throw new ModelArchitectException(sprintf(
                'Unable to complete model. I don\'t have supplied these properties: %s',
                $notSupplied
            ));
        }
        $model = $this->model;
        $modelProps = $this->suppliedProps;

        $this->propsToSupply = [];
        $this->suppliedProps = [];

        return new $model($modelProps);
    }

    /**
     * Determines whether property value is valid
     * @param mixed $value
     * @param string $valueType
     * @return boolean
     */
    private function isPropValueValid($value, $valueType)
    {
        return $this->$valueType($value);
    }

    /**
     * 
     * @param string $propType
     * @return boolean
     */
    private function isPropArrayOfValues(string $propType)
    {
        return strpos($propType, '[]') !== false;
    }

    /**
     * @return boolean
     */
    private function canModelBeFinished()
    {
        return empty($this->whatAttributesToSupply());
    }

}
