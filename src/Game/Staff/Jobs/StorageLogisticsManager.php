<?php

namespace Vesmirno\Game\Staff\Jobs;

use Vesmirno\Game\Objects\Manufacturable\ManufacturableInterface;

class StorageLogisticsManager extends Job
{
    /** @var ManufacturableInterface[] */
    protected $storage = [];

    /**
     * Add given product(factorable class) to stock
     * @param ManufacturableInterface $product
     */
    public function addToStock(ManufacturableInterface $product = null)
    {
        if ($product) {
            $key = get_class($product);
            if (array_key_exists($key, $this->storage)) {
                $this->storage[$key]['count'] = $this->storage[$key]['count'] + 1;
                $this->storage[$key]['instances'][] = $product;
            } else {
                $this->storage[$key] = [
                    'count' => 1,
                    'instances' => [$product]
                ];
            }
        }
    }

    /**
     * Remove needed supply factorable class instance from stock
     * @param string $product
     * @return ManufacturableInterface
     */
    public function removeFromStock(string $product) : ManufacturableInterface
    {
        if (array_key_exists($product, $this->storage)) {
            $countInStock = $this->storage[$product]['count'] - 1;

            $item = end($this->storage[$product]['instances']);

            if ($countInStock == 0) {
                unset($this->storage[$product]);
            } else {
                array_pop($this->storage[$product]['instances']);
                $this->storage[$product]['count'] -= 1;
            }

            return $item;
        }
        //throw error
    }

    /**
     * Checks if needen part is available in stock
     * @param string $product
     * @return boolean
     */
    public function isInStock(string $product): bool
    {
        return array_key_exists($product, $this->storage);
    }

    /**
     * Returns storage content
     * @return array
     */
    public function storagePreview()
    {
        return $this->storage;
    }

    /**
     * Returns unused supplied part to stock
     * @param ManufacturableInterface[] $unusedSupplies
     */
    public function returnToStock(array $unusedSupplies = [])
    {
        foreach ($unusedSupplies as $supply) {
            $this->addToStock($supply);
        }
    }

}
