<?php

namespace Vesmirno\Building\BuildingServices;

use Vesmirno\Game\Objects\Buildable\Buildings\BuildingInterface;
use Vesmirno\Game\Staff\Jobs\JobInterface;
use Vesmirno\Game\Staff\Management\BuildingServiceInterface;
use Vesmirno\Game\Staff\People\PersonInterface;


class Management implements BuildingServiceInterface
{

    /** @var BuildingInterface */
    private $building;

    /** @var PersonInterface[] */
    private $managers = [];

    /**
     * Management constructor.
     * @param PersonInterface[] $managers
     */
    public function __construct(array $managers)
    {
        foreach ($managers as $manager) {
            $this->managers[get_class($manager->getJob())] = $manager;
        }
    }

    /**
     * Calls upon manager competence
     * @param string $name
     * @param array $arguments
     * @return Management|mixed
     */
    public function __call($name, $arguments)
    {
        if ($this->building) {
            foreach ($this->managers as $manager) {
                if (in_array($name, $manager->getJob()->competences())) {
                    $result = call_user_func_array([$manager, $name], $arguments);

                    return $result ?? $this;
                }
            }
            //error, building is not capable of doing so
        }
        echo 'Managers unable to carry out their work, until they\'re assigned to a building!';
    }

    /**
     * Assigns management to building
     * @param BuildingInterface $building
     * @return Management
     */
    public function assignToBuilding(BuildingInterface $building)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Returns particular manager from building management
     * @param string $manager
     * @return JobInterface|null
     */
    public function getManager(string $manager)
    {
        return $this->factoryManagers[$manager] ?? null;
    }

    protected function requiredManagers()
    {
        return [];
    }

}
