<?php

namespace Vesmirno\Building\BuildingServices;

use Vesmirno\Game\Objects\Industrial\AssemblyLine;
use Vesmirno\Game\Staff\Jobs\FactoryJobs\FactoryProductionManager;
use Vesmirno\Game\Staff\Jobs\StorageLogisticsManager;

/**
 * @method FactoryManagement addAssemblyLine(string $product, string $lineName = null) Adds assembly line to assembly lines array
 * @method [] getAssemblyLineNames() Gets assembly lines names
 * @method AssemblyLine getAssemblyLineByName(string $name)
 * @method AssemblyLine[] getAssemblyLines() Gets assembly line array
 * @method FactoryManagement setAssemblyLineProductionPriorityWeight(int $priority, string $name = null) Sets assembly line produciton priority
 * @method FactoryManagement renameAssemblyLine(string $originalName, string $newName) Renames assembly line
 * @method [] getAssemblyLinePriorities() Gets assembly lines priority array
 */

class FactoryManagement extends Management
{
    public function requiredManagers()
    {
        return [
            FactoryProductionManager::class,
            StorageLogisticsManager::class
        ];
    }
}
