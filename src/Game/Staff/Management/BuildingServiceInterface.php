<?php

namespace Vesmirno\Game\Staff\Management;

use Vesmirno\Game\Objects\Buildable\Buildings\BuildingInterface;
use Vesmirno\Game\Staff\Jobs\JobInterface;

interface BuildingServiceInterface
{
    /**
     * @param BuildingInterface $building
     * @return BuildingInterface
     */
    public function assignToBuilding(BuildingInterface $building);

    /**
     * @param string $manager
     * @return JobInterface
     */
    public function getManager(string $manager);
}
