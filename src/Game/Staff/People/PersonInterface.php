<?php

namespace Vesmirno\Game\Staff\People;

use Vesmirno\Game\Staff\Jobs\JobInterface;

interface PersonInterface
{
    /**
     * @return JobInterface
     */
    public function getJob(): JobInterface;

    /**
     * @param JobInterface $job
     * @return $this
     */
    public function setJob(JobInterface $job): PersonInterface;

    /**
     * @return bool
     */
    public function hasJob(): bool;

    /**
     * @return PersonInterface
     */
    public function removeJob(): PersonInterface;

}