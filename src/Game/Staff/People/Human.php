<?php

namespace Vesmirno\Game\Staff\People;

use Vesmirno\Traits\GenderTrait;

class Human extends Person
{
    use GenderTrait;

    public const GENDER_MALE = "male";
    public const GENDER_FEMALE = "female";

    public function __construct(string $gender)
    {
        $this->setGender($gender);
        parent::__construct();
    }
}