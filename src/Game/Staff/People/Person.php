<?php

namespace Vesmirno\Game\Staff\People;

use Vesmirno\Game\Game;
use Vesmirno\Game\GameObject;
use Vesmirno\Game\Staff\Jobs\JobInterface;
use Vesmirno\Interfaces\Tools\ToolInterface;
use Vesmirno\Traits\Metrics\AgeTrait;
use Vesmirno\Traits\Metrics\HeightTrait;
use Vesmirno\Traits\Metrics\WeightTrait;
use Vesmirno\Traits\NameTrait;
use Vesmirno\Traits\Physical\StrengthTrait;

class Person extends GameObject implements PersonInterface
{
    use HeightTrait, WeightTrait, AgeTrait, NameTrait, StrengthTrait;

    /** @var JobInterface|null */
    protected ?JobInterface $job = null;

    /** @var array */
    protected array $professionTrait;

    public function __construct(
        string $name = null,
        int $age = 0,
        float $weight = 0,
        float $height = 0,
        int $strength = 0
    )
    {
        if ($name) {
            $this->setName($name);
        }
        $this->setWeight($weight);
        $this->setHeight($height);
        $this->setAge($age);
        $this->setStrength($strength);
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function getJob(): JobInterface
    {
        return $this->job;
    }

    /**
     * @inheritDoc
     */
    public function setJob(JobInterface $job): PersonInterface
    {
        $this->job = $job;
        $this->professionTrait = $job->getProfessionTrait();

        return $this;
    }

    /**
     * @param string $objectId
     * @return string|null
     */
    public function examine(string $objectId): ?string
    {
        //TODO: should be affected via education and current profession

        $object = Game::gameObjectRegister()->getObject($objectId);
        if (!empty($object)) {
            if (method_exists($this->professionTrait, __METHOD__)) {
                return call_user_func_array([$this->professionTrait, __METHOD__], [$object]);
            }
            return get_class($object);
        }

        return null;
    }

    /**
     * @return bool
     */
    public function hasJob(): bool
    {
        return !empty($this->job);
    }

    /**
     * @return PersonInterface
     */
    public function removeJob(): PersonInterface
    {
        $this->job = null;

        return $this;
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->job, $name)) {
            foreach ($arguments as $argument) {
                if (in_array(ToolInterface::class, class_implements($argument))) {
                    $this->job->addTool($argument);
                }
            }
            return call_user_func_array([$this->job, $name], $arguments);
        }

        return null;
    }
}