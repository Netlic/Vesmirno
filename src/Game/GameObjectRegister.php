<?php

namespace Vesmirno\Game;

use Vesmirno\Helper\StringHelper;

//TODO: read cipher options from config
class GameObjectRegister
{
    /** @var object[] */
    private array $registeredObjects = [];

    /** @var string */
    private string $cipherKey;

    /** @var int */
    private int $cipherKeyLength = 5;

    /** @var string */
    private string $encryptedClassDelimiter;

    public function __construct()
    {
        $this->cipherKey = StringHelper::generateRandomString($this->cipherKeyLength);
        $this->encryptedClassDelimiter = StringHelper::generateRandomString($this->cipherKeyLength);
    }

    /**
     * @param object $object
     */
    public function addObject(object $object)
    {
        $objectId = static::createObjectHash(get_class($object));
        $this->registeredObjects[$objectId] = $object->setObjectId($objectId);
    }

    /**
     * @return object[]
     */
    public function listObjects()
    {
        return $this->registeredObjects;
    }

    /**
     * @param string $objectId
     * @return object|null
     */
    public function getObject(string $objectId)
    {
        return $this->registeredObjects[$objectId] ?? null;
    }

    /**
     * @param string $objectClass
     * @return string
     */
    private function createObjectHash(string $objectClass): string
    {
        return openssl_encrypt(sprintf('%s%s%s',
            StringHelper::generateRandomString(),
            uniqid(time(), true),
            $this->encryptedClassDelimiter
        ), 'AES-256-CFB1', $this->cipherKey, 0, openssl_random_pseudo_bytes(16));
    }
}