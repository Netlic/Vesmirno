<?php

namespace Vesmirno\Game;

use Vesmirno\Traits\GameObjectTrait;

abstract class GameObject
{
    use GameObjectTrait;

    public function __construct()
    {
        Game::gameObjectRegister()->addObject($this);
    }

    public function creationInstructions()
    {

    }
}