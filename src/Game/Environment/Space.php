<?php

namespace Vesmirno\Game\Environment;

use Vesmirno\Game\GameObject;
use Vesmirno\Traits\Metrics\DepthTrait;
use Vesmirno\Traits\Metrics\HeightTrait;
use Vesmirno\Traits\Metrics\WidthTrait;

class Space extends GameObject
{
    use HeightTrait, WidthTrait, DepthTrait;
}