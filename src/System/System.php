<?php

namespace Vesmirno\System;

use Vesmirno\Common\CompositeFunctionality;

/**
 * Class SystemData
 * @package Vesmirno\System
 * @method string getGamePath()
 */
class System extends CompositeFunctionality
{
    /** @var Path */
    protected $path;

    /** @var array */
    protected $doNotProcess = [
        '.', '..'
    ];

    /**
     * @inheritDoc
     */
    protected function relatedInstanceClass(): string
    {
        return GameSystemClass::class;
    }

    public function translatePath($path)
    {
        $pathArray = explode("/", $path);
        foreach ($pathArray as $index => $pathPart) {
            $createdPathPartMethod = 'get' . str_replace(' ', '', ucwords(str_replace('-', ' ', $pathPart)));
            if (!empty($translatedPath = $this->$createdPathPartMethod($path))) {
                $pathArray[$index] = $translatedPath;
            }
        }

        return implode('/', $pathArray);
    }

    /**
     * @param string $path
     * @param callable $closure
     * @param null $breakIfReturn
     * @return mixed|null
     */
    public function iterateFilesInDirectory(string $path, callable $closure, $breakIfReturn = null)
    {
        $path = $this->translatePath($path);
        if (file_exists($path) && $handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {
                if (!in_array($entry, $this->doNotProcess)) {
                    if (is_dir($path . '/' . $entry)) {
                        $this->iterateFilesInDirectory($path . '/' . $entry, $closure);

                        continue;
                    }

                    $result = $closure($path . '/' . $entry);

                    $hasProperReturnValue = false;
                    if (!empty($breakIfReturn) && !empty($result)) {
                        if (class_exists($breakIfReturn) && is_subclass_of($result, $closure)) {
                            $hasProperReturnValue = true;
                        }

                        if (interface_exists($breakIfReturn) && in_array($breakIfReturn, class_implements($result))) {
                            $hasProperReturnValue = true;
                        }

                        if (gettype($result) == $breakIfReturn) {
                            $hasProperReturnValue = true;
                        }

                        if ($hasProperReturnValue) {
                            closedir($handle);

                            return $result;
                        }
                    }

                }
            }
            closedir($handle);
        }

        return null;
    }
}