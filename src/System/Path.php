<?php

namespace Vesmirno\System;

class Path implements GameSystemClass
{
    /**
     * @return string
     */
    public function getGamePath(): string
    {
        return dirname(__DIR__, 2);
    }
}