<?php

namespace Vesmirno\Common;

abstract class CompositeFunctionality
{
    public function __construct(array $data)
    {
        foreach ($data as $key => $simpleData) {
            $this->$key = $simpleData;
        }
    }

    public function __call($name, $arguments)
    {
        $instance = $this->relatedInstanceClass();
        foreach (get_object_vars($this) as $attribute) {
            if (gettype($attribute) === 'object' && $attribute instanceof $instance && method_exists($attribute, $name)) {
                return call_user_func_array([$attribute, $name], $arguments);
            }
        }

        return null;
    }

    /**
     * @return string
     */
    protected abstract function relatedInstanceClass(): string;

}