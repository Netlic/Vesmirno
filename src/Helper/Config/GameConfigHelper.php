<?php

namespace Vesmirno\Helper\Config;

use Vesmirno\Traits\CompositeTrait;

class GameConfigHelper implements ConfigInterface
{
    use CompositeTrait;

    /** @var array */
    protected $config;

    public function __construct(string $config)
    {
        //TODO: deal with json error
        $this->config = json_decode($config, true);
        if (json_last_error() !== 0) {
            // throw error json_last_error_msg()
        }
    }

    /**
     * @inheritDoc
     */
    public function get(string $key, $default = null)
    {
        if (strpos($key, '.')) {
            $keys = explode('.', $key);
            $value = $this->config;
            foreach ($keys as $configKey) {
                if (empty($value[$configKey])) {
                    return $default;
                }

                $value = $value[$configKey];
            }

            return $value;
        }

        return empty($this->config[$key]) ? $default : $this->config[$key];
    }

}