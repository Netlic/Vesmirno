<?php

namespace Vesmirno\Helper;

/**
 * Really simple but helpful model calculations
 */
class ModelCalculationHelper
{

    /**
     * 
     * @return array
     */
    public static function calculateHeight()
    {
        return ['prop' => 'height', 'value' => count(func_get_args()) * 1.2];
    }

    /**
     * 
     * @return array
     */
    public static function calculateWidth()
    {
        return ['prop' => 'width', 'value' => count(func_get_args()) * 1.1];
    }

}
