<?php

namespace Vesmirno\Helper;

class StringHelper
{
    /**
     * @param int $length
     * @param string $keyspace
     * @return string
     */
    public static function generateRandomString(int $length = 64, string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $pieces = [];
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces []= $keyspace[static::randomInt(0, $max)];
        }

        return implode('', $pieces);
    }

    /**
     * @param int $min
     * @param int $max
     * @return int
     */
    private static function randomInt(int $min, int $max): int
    {
        try {
            return random_int($min, $max);
        } catch (\Exception $e) {
            return static::randomInt($min, $max);
        }
    }
}