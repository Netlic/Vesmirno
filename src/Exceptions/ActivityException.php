<?php

namespace Vesmirno\Exceptions;

use Throwable;
use Vesmirno\Commands\CommandInterface;

class ActivityException extends VesmirnoException
{
    public const EXCEPTION_CODE = '1';

    /** @var CommandInterface */
    private $commandEntity;

    public function __construct(string $message, CommandInterface $commandEntity, Throwable $previous = null)
    {
        $this->commandEntity = $commandEntity;
        parent::__construct($message, $previous);
    }

    /**
     * @inheritDoc
     */
    protected function exceptionCode(): int
    {
        $class = get_class($this->commandEntity);
        $commandId = call_user_func([$class, 'commandId']);
        $code = '';
        for ($i = 0; $i < strlen($commandId); $i ++) {
            $code .= (string)$commandId[$i];
        }

        return (int) sprintf ('%s%s', self::EXCEPTION_CODE, $code);
    }
}