<?php

namespace Vesmirno\Exceptions;

use Throwable;

abstract class VesmirnoException extends \Exception
{
    public function __construct($message = "Something is wrong with our universe", Throwable $previous = null)
    {
        parent::__construct($message, $this->exceptionCode(), $previous);
    }

    /**
     * @return int
     */
    protected abstract function exceptionCode(): int;
}