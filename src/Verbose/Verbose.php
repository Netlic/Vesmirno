<?php

namespace Vesmirno\Verbose;

use Vesmirno\Common\CompositeFunctionality;
use Vesmirno\Traits\TranslatorTrait;

/**
 * Class Verbose
 * @package Vesmirno\Verbose
 * @method string askForCommand()
 * @method string commandBuilderBuild()
 * @method string commandGodCreate()
 * @method string commandUnknownActivity()
 * @method string commandWhatCanIdo()
 * @method string errorArgumentNotSupplied()
 * @method string errorUnknownArgument()
 * @method string errorUnnecessaryArgument()
 */
class Verbose extends CompositeFunctionality
{
    use TranslatorTrait;

    /** @var GameEssentialVerbose */
    protected $gameEssentialVerbose;

    /** @var CommandVerbose */
    protected $commandVerbose;

    /** @var VerboseBuffer */
    protected $verboseBuffer;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->initiateTranslator();
        $this->verboseBuffer = new VerboseBuffer();
    }

    public function __call($name, $arguments)
    {
        $result = parent::__call($name, $arguments);

        return empty($result) ? null : $this->t->__($result);
    }

    /**
     * @return VerboseBuffer
     */
    public function getVerboseBuffer(): VerboseBuffer
    {
        return $this->verboseBuffer;
    }

    /**
     * @inheritDoc
     */
    protected function relatedInstanceClass(): string
    {
        return VerboseInterface::class;
    }
}