<?php

namespace Vesmirno\Verbose;

use Vesmirno\Traits\Command\Builder;
use Vesmirno\Traits\Command\God;
use Vesmirno\Traits\TranslatorTrait;

class CommandVerbose implements VerboseInterface
{
    use TranslatorTrait, Builder, God;

    public function __construct()
    {
        $this->initiateTranslator();
    }

    /**
     * @return string
     */
    public function commandWhatCanIdo()
    {
        return $this->t->__('I can do the following:');
    }

    /**
     * @return string
     */
    public function errorArgumentNotSupplied()
    {
        return $this->t->__('activity argument not supplied');
    }

    /**
     * @return string
     */
    public function errorUnknownArgument()
    {
        return $this->t->__('what did you gave to me?');
    }

    /**
     * @return string
     */
    public function errorUnnecessaryArgument()
    {
        return $this->t->__('I don\'t need this');
    }
}