<?php

namespace Vesmirno\Verbose;

class VerboseBuffer
{
    /** @var string */
    private $bufferedText;

    /**
     * @param string $text
     * @return VerboseBuffer
     */
    public function addLineToBuffer(string $text): VerboseBuffer
    {
        return $this->addToBuffer($text)->addToBuffer(PHP_EOL);
    }

    /**
     * @param string $text
     * @return VerboseBuffer
     */
    public function addToBuffer(string $text): VerboseBuffer
    {
        $this->bufferedText .= $text;

        return $this;
    }

    /**
     * @return string
     */
    public function emptyBuffer(): string
    {
        $buffer = $this->bufferedText;
        $this->bufferedText = '';

        return $buffer;
    }

    /**
     * @return string
     */
    public function getBufferContent(): string
    {
       return $this->bufferedText;
    }
}