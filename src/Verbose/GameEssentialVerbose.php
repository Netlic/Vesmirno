<?php

namespace Vesmirno\Verbose;

class GameEssentialVerbose implements VerboseInterface
{
    public function askForCommand()
    {
        return 'Enter_command:';
    }
}