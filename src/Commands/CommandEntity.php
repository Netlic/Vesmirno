<?php

namespace Vesmirno\Commands;

use Vesmirno\Commands\Arguments\ActivityArgument;
use Vesmirno\Exceptions\ActivityException;
use Vesmirno\Game\Game;
use Vesmirno\Traits\TranslatorTrait;
use Vesmirno\Verbose\Verbose;

abstract class CommandEntity implements CommandInterface
{
    use TranslatorTrait;

    /** @var Verbose */
    protected $verbose;

    /** @var ActivityArgument[]*/
    protected $arguments = [];

    public function __construct()
    {
        $this->verbose = Game::verbose();
        $this->initiateTranslator();
    }

    /**
     * @inheritDoc
     */
    public function unknownWork()
    {
        $this->verbose->getVerboseBuffer()->addToBuffer($this->verbose->commandUnknownActivity());
    }

    /**
     * @inheritDoc
     */
    public function addActivityArgument(ActivityArgument $argument): CommandInterface
    {
        if ($argument instanceof ActivityArgument) {
            $this->arguments[$argument->getShortcut()] = $argument;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getActivityArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @param string $activity
     * @throws ActivityException
     */
    public function executeActivity(string $activity)
    {
        if (!method_exists($this, $activity)) {
            $activity = 'unknownWork';
        }

        $arguments = [];
        if (!empty($this->getActivityArgumentsMapping()[$activity])) {
            $toSupplyArguments = array_keys($this->getActivityArgumentsMapping()[$activity]);
            $suppliedArguments = array_keys($this->getActivityArguments());
            if (!empty(array_diff($toSupplyArguments, $suppliedArguments))) {
                throw new ActivityException($this->verbose->errorArgumentNotSupplied(), $this);
            }

            foreach($this->getActivityArguments() as $activityArgument) {
                $arguments[] = $activityArgument->getValue();
            }
        }
        $this->verbose->getVerboseBuffer()
            ->addToBuffer(PHP_EOL)
            ->addToBuffer(sprintf('%s: ', $this->t->__(ucfirst(static::commandId()))));

        call_user_func_array([$this, $activity], $arguments);
    }

    /**
     * @return void
     */
    public function whatCanIdo()
    {
        $verboseBuffer = $this->verbose->getVerboseBuffer();
        $verboseBuffer->addLineToBuffer($this->verbose->commandWhatCanIdo());
        $tab = '    ';
        foreach($this->getCommandActivityMethodsWithDescriptions() as $activityName => $describedActivity) {
            $activityForSpacing = $activityName;
            if (is_int($activityName)) {
                $activityForSpacing = $describedActivity;
                $describedActivity = "";
            }
            $spacedActivity = $this->t->__(strtolower(preg_replace('/(?<!\ )[A-Z]/', ' $0', $activityForSpacing)));
            $verboseBuffer->addLineToBuffer(sprintf('%s- %s%s%s', $tab, $spacedActivity, $tab, $this->t->__($describedActivity)));
        }
    }

    protected abstract function getCommandActivityMethodsWithDescriptions(): array;
}