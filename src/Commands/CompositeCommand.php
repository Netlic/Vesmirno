<?php

namespace Vesmirno\Commands;

use Vesmirno\Commands\Arguments\ActivityArgument;
use Vesmirno\Commands\Entities\Builder;
use Vesmirno\Common\CompositeFunctionality;
use Vesmirno\Exceptions\ActivityException;
use Vesmirno\Game\Game;
use Vesmirno\Traits\TranslatorTrait;
use Vesmirno\Verbose\VerboseBuffer;

class CompositeCommand extends CompositeFunctionality
{
    use TranslatorTrait;

    /** @var Builder */
    protected Builder $builderCommand;

    /** @var CommandInterface[] */
    protected array $data = [];

    /** @var string */
    protected string $emptyActivity = 'whatCanIdo';

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->initiateTranslator();
    }

    /**
     * @param string $command
     * @return bool
     */
    public function execute(string $command): bool
    {
        $entityActivity = '';
        if (!$this->validateCommand($command)) {
            $this->getVerboseBuffer()->addLineToBuffer($this->t->__('Invalid command'));
            return false;
        }

        $commandId = $this->t->__(strtok($command, ':'));
        $commandObj = $this->findByCommandId($commandId);

        if (empty($commandObj)) {
            $this->getVerboseBuffer()->addLineToBuffer(sprintf('%s: "%s"', $this->t->__(''), $entityActivity));
            return false;
        }

        $spaces = $this->t->__(strtok(' '));
        while ($spaces !== false) {
            if ((bool)$this->isActivityPart($spaces)) {
                $entityActivity .= ucfirst($this->t->__(trim($spaces)));
            } else {
                try {
                    $this->addActivityArgument($spaces, $commandObj, $entityActivity);
                } catch (ActivityException $e) {
                    $this->getVerboseBuffer()->addLineToBuffer($e->getMessage());
                }
            }
            $spaces = strtok(' ');
        }

        $entityActivity = lcfirst($entityActivity);
        if (strlen(trim($entityActivity)) === 0) {
            $entityActivity = $this->emptyActivity;
        }

        try {
            $commandObj->executeActivity($entityActivity);
        } catch (ActivityException $e) {
            $this->getVerboseBuffer()->addToBuffer($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * @param string $command
     * @return bool
     */
    protected function validateCommand(string $command): bool
    {
        return !preg_match('/[A-Za-z0-9](-)(=)(:)/', $command);
    }

    /**
     * @param string $commandPart
     * @return bool
     */
    protected function isActivityPart(string $commandPart): bool
    {
        return !preg_match('/[^A-Za-z0-9]/', $commandPart);
    }

    /**
     * @param string $commandId
     * @return CommandInterface|null
     */
    protected function findByCommandId(string $commandId): ?CommandInterface
    {
        if (!empty($this->data[$commandId])) {
            return $this->data[$commandId];
        }
        $commandMappings = Game::config()->get('command.command-mapping');
        if (!empty($commandMappings[$commandId])) {
            $classFromMapping = $commandMappings[$commandId];
            if ($this->implementsInterface($classFromMapping)) {
                return $this->data[$commandId] = new $classFromMapping;
            }
        }

        $instance = &$this;
        foreach (Game::config()->get('command.command-paths') as $path) {
            Game::system()->iterateFilesInDirectory($path, function ($file) use ($instance, $commandId) {
                include_once $file;
                $declaredClasses = get_declared_classes();
                $commandClass = end($declaredClasses);
                if ($this->implementsInterface($commandClass) && is_callable([$commandClass, 'commandId'])) {
                    $containedCommandId = call_user_func([$commandClass, 'commandId']);
                    $instance->data[$containedCommandId] = new $commandClass;
                }
            }, $this->relatedInstanceClass());
            if (!empty($this->data[$commandId])) {
                return $this->data[$commandId];
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    protected function relatedInstanceClass(): string
    {
        return CommandInterface::class;
    }

    /**
     * @param string $argument
     * @param CommandInterface $commandEntity
     * @param string $entityActivity
     * @throws ActivityException
     */
    protected function addActivityArgument(string $argument, CommandInterface $commandEntity, string $entityActivity): void
    {
        $mapping = $commandEntity->getActivityArgumentsMapping();
        $argument = ActivityArgument::createArgument($argument);

        if (empty($argument)) {
            throw new ActivityException(Game::verbose()->errorUnknownArgument(), $commandEntity);
        }

        if (empty($mapping[lcfirst($entityActivity)][$argument->getShortcut()])) {
            throw new ActivityException(sprintf('%s: %s', Game::verbose()->errorUnnecessaryArgument(), $argument), $commandEntity);
        }

        $commandEntity->addActivityArgument($argument);
    }

    /**
     * @param string $class
     * @return bool
     */
    private function implementsInterface(string $class)
    {
        $interfaces = class_implements($class);

        return $interfaces && in_array($this->relatedInstanceClass(), $interfaces);
    }

    /**
     * @return VerboseBuffer
     */
    private function getVerboseBuffer()
    {
        return Game::verbose()->getVerboseBuffer();
    }
}