<?php

namespace Vesmirno\Commands;

use Vesmirno\Commands\Arguments\ActivityArgument;
use Vesmirno\Exceptions\ActivityException;

interface CommandInterface
{
    /**
     * @return void
     */
    public function unknownWork();

    /**
     * @return array
     */
    public function getActivityArgumentsMapping(): array;

    /**
     * @param ActivityArgument $argument
     * @return CommandInterface
     */
    public function addActivityArgument(ActivityArgument $argument): CommandInterface;

    /**
     * @return ActivityArgument[]
     */
    public function getActivityArguments(): array;

    /**
     * @param string $activity
     * @throws ActivityException
     */
    public function executeActivity(string $activity);

    /**
     * @return void
     */
    public function whatCanIdo();

    /**
     * @return string
     */
    public static function commandId(): string;
}