<?php

namespace Vesmirno\Commands\Entities;

use Vesmirno\Commands\CommandEntity;

class Architect extends CommandEntity
{

    /**
     * @inheritDoc
     */
    public function getActivityArgumentsMapping(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public static function commandId(): string
    {
        return 'architect';
    }

    public function getAvailableMaterial()
    {
        return "ble";
    }

    protected function getCommandActivityMethodsWithDescriptions(): array
    {
        return ['getAvailableMaterial' => 'shows material which can be used to construct a building'];
    }
}