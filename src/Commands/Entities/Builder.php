<?php

namespace Vesmirno\Commands\Entities;

use Vesmirno\Commands\CommandEntity;
use Vesmirno\Commands\CommandInterface;

class Builder extends CommandEntity implements CommandInterface
{
    /**
     * @return string
     */
    public function build()
    {
        $this->verbose->getVerboseBuffer()->addToBuffer($this->verbose->commandBuilderBuild());
        return $this->verbose->commandBuilderBuild();
    }

    /**
     * @inheritDoc
     */
    public static function commandId(): string
    {
        return "builder";
    }

    /**
     * @inheritDoc
     */
    public function getActivityArgumentsMapping(): array
    {
        return [];
    }

    protected function getCommandActivityMethodsWithDescriptions(): array
    {
        return ['build' => 'commences building structure'];
    }
}