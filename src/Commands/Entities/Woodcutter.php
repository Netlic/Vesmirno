<?php

namespace Vesmirno\Commands\Entities;

use Vesmirno\Commands\CommandEntity;
use Vesmirno\Game\Objects\Growers\Plants\Trees\TreeInterface;

class Woodcutter extends CommandEntity
{
    public function examine(TreeInterface $tree)
    {

    }

    public function cut(TreeInterface $tree)
    {
        var_dump(get_class($tree));
    }

    public function createLogs(TreeInterface $tree)
    {
        $this->cut($tree);
    }

    public function createPlanks(TreeInterface $tree)
    {
        $this->cut($tree);
    }

    public function createFireWood(Object $source)
    {
        if ($this->isTree($source)) {
            $this->cut($source);
        }

        if ($this->isWoodResource($source)) {

        }

        return null;
    }

    protected function getCommandActivityMethodsWithDescriptions(): array
    {
        return [
            'cut' => 'just point on a tree and it\'s going down',
            'examine' => 'I can give you more insight about the tree',
            'createLogs' => 'I will provide you with the wooden logs, for various use',
            'createPlanks' => 'Just another useful product from a tree',
            'createFireWood' => 'Wanna have something cozy?'
        ];
    }

    /**
     * @inheritDoc
     */
    public function getActivityArgumentsMapping(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public static function commandId(): string
    {
        return 'woodcutter';
    }

    /**
     * @param Object $unknownResource
     * @return bool
     */
    private function isTree(Object $unknownResource)
    {
        return true;
    }

    /**
     * @param Object $unknownResource
     * @return bool
     */
    private function isWoodResource(Object $unknownResource)
    {
        return true;
    }
}