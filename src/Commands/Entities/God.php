<?php

namespace Vesmirno\Commands\Entities;

use Vesmirno\Commands\CommandEntity;
use Vesmirno\Game\Game;

class God extends CommandEntity
{
    /**
     * @param string $class
     * @return object
     */
    public function create(string $class): object
    {
        //TODO: check if class exists and extends GameObject class
        if (class_exists($class)) {
            try {
                $object = new $class();
                $classArray = explode('\\', $class);
                $this->verbose->getVerboseBuffer()->addToBuffer(
                    sprintf('%s %s', $this->verbose->commandGodCreate(), lcfirst(end($classArray)))
                );
                return $object;
            } catch (\Exception $e) {
                //throw
            }

        }

        //throw
    }

    /**
     * @return object[]
     */
    public function listObjects()
    {
        return Game::gameObjectRegister()->listObjects();
    }

    /**
     * @inheritDoc
     */
    protected function getCommandActivityMethodsWithDescriptions(): array
    {
        return [
            'create' => 'the power to create anything'
        ];
    }

    /**
     * @inheritDoc
     */
    public function getActivityArgumentsMapping(): array
    {
        return [
            'create' => [
                'o' => 'object'
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public static function commandId(): string
    {
        return 'god';
    }
}