<?php

namespace Vesmirno\Commands\Entities;

use Vesmirno\Commands\CommandEntity;
use Vesmirno\Game\Staff\Jobs\JobInterface;
use Vesmirno\Game\Staff\People\PersonInterface;

class Recruiter extends CommandEntity
{
    public function train(PersonInterface $person, JobInterface $job)
    {

    }

    public function findSuitablePerson()
    {

    }

    protected function getCommandActivityMethodsWithDescriptions(): array
    {
        // TODO: Implement getCommandActivityMethodsWithDescriptions() method.
    }

    /**
     * @inheritDoc
     */
    public function getActivityArgumentsMapping(): array
    {
        // TODO: Implement getActivityArgumentsMapping() method.
    }

    /**
     * @inheritDoc
     */
    public static function commandId(): string
    {
        return 'recruiter';
    }
}