<?php

namespace Vesmirno\Commands\Arguments;

class ActivityArgument
{
    /** @var string */
    private $shortcut;

    /** @var mixed */
    private $value;

    /**
     * @param string $textualArgument
     * @return ActivityArgument|null
     */
    public static function createArgument(string $textualArgument): ?ActivityArgument
    {
        if (strpos($textualArgument, '=') !== false && strpos($textualArgument, '--')) {
            return null;
        }

        $arrayArgument = explode('=', str_replace("--", "", $textualArgument));
        return new static(reset($arrayArgument), end($arrayArgument));
    }

    public function __construct(string $shortcut, $value)
    {
        $this->shortcut = $shortcut;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getShortcut()
    {
        return $this->shortcut;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%s=%s', $this->shortcut, $this->value);
    }
}