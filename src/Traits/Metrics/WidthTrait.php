<?php

namespace Vesmirno\Traits\Metrics;

trait WidthTrait
{
    /** @var float */
    protected $width;

    /**
     * @param float $width
     * @return $this
     */
    public function setWidth(float $width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }
}