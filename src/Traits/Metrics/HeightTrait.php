<?php

namespace Vesmirno\Traits\Metrics;

trait HeightTrait
{
    /** @var float */
    protected $height;

    /**
     * @param float $height
     * @return $this
     */
    public function setHeight(float $height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }
}