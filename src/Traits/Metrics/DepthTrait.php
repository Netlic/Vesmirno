<?php

namespace Vesmirno\Traits\Metrics;

trait DepthTrait
{
    /** @var float */
    protected $depth;

    /**
     * @return float
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param float $depth
     * @return $this
     */
    public function setDepth(float $depth)
    {
        $this->depth = $depth;

        return $this;
    }
}