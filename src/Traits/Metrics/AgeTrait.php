<?php

namespace Vesmirno\Traits\Metrics;

trait AgeTrait
{
    /** @var float */
    protected $age;

    /**
     * @param float $age
     * @return $this
     */
    public function setAge(float $age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return float
     */
    public function getAge(): float
    {
        return $this->age;
    }
}