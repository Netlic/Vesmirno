<?php

namespace Vesmirno\Traits\Metrics;

trait WeightTrait
{
    protected $weight;

    /**
     * @param float $weight
     * @return $this
     */
    public function setWeight(float $weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }
}