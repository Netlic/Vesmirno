<?php

namespace Vesmirno\Traits\Physical;

trait StrengthTrait
{
    /** @var int */
    protected $strength;

    /**
     * @param int $strength
     * @return $this
     */
    public function setStrength(int $strength)
    {
        $this->strength = $strength;

        return $this;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }
}