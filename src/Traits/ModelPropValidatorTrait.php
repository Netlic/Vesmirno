<?php

namespace Vesmirno\Traits;

trait ModelPropValidatorTrait
{

    /**
     * Validates float value
     * @param float $value
     * @return boolean
     */
    public function isFloat($value)
    {
        if (!is_numeric($value)) {
            return false;
        }

        return is_float((float) $value);
    }

    /**
     * Validates int value
     * @param int $value
     * @return boolean
     */
    public function isInt($value)
    {
        if (!is_numeric($value)) {
            return false;
        }

        return is_float((float) $value);
    }

    /**
     * Return bool if given property value is instance of given object
     * @param string $className
     * @param object $value
     * @return boolean
     */
    public function isInstanceOfClass(string $className, $value)
    {
        return is_object($value) && (get_class($value) == $className);
    }

    /**
     * 
     * @param string $interfaceName
     * @param object $value
     * @return boolean
     */
    public function implementsInterface(string $interfaceName, $value)
    {
        return is_object($value) && in_array($interfaceName, class_implements($value));
    }

    public function __call($name, $arguments)
    {
        $methodName = 'is' . ucfirst($name);
        if (method_exists($this, $methodName)) {
            if (count($arguments) == 1) {
                $arguments = reset($arguments);
            }

            return call_user_func_array([$this, $methodName], [$arguments]);
        }

        if (class_exists($name)) {
            return $this->isInstanceOfClass($name, reset($arguments));
        }

        if (interface_exists($name)) {
            return $this->implementsInterface($methodName, reset($arguments));
        }

        return true;
    }

}
