<?php

namespace Vesmirno\Traits;

trait GenderTrait
{
    /** @var string[] */
    protected $genderNames = ['female', 'male'];

    /** @var string */
    protected $gender;

    public function setGender(string $gender)
    {
        if ($this->validateGender($gender)) {
            $this->gender = $gender;

            return $this;
        }

        //TODO:throw error
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return bool
     */
    private function validateGender(string $gender)
    {
        return in_array($gender, $this->genderNames);
    }
}