<?php

namespace Vesmirno\Traits;

use Vesmirno\Slots\VehicleSlot\ShieldEmmiterSlot;

/**
 *
 * @author brano
 */
trait ShieldTrait
{

    /** @var ShieldEmmiterSlot[] */
    protected $shieldSlots = [];

    /**
     * 
     * @return ShieldEmmiterSlot[]
     */
    public function getShieldSlots()
    {
        return $this->shieldSlots;
    }

    /**
     * Gets the shield slots available on vehicle
     * @return int
     */
    public function getShieldSlotsCount() : int
    {
        if (method_exists($this, 'getItemSlotsCount')) {
            return $this->getItemSlotsCount(null, $this->shieldSlots);
        }

        return count($this->shieldSlots);
    }

    /**
     * 
     * @return ShieldSlot[]
     */
    public function getEmptyShieldSlots()
    {
        if (method_exists($this, 'getEmptyItemSlots')) {
            return $this->getEmptyItemSlots(null, $this->shieldSlots);
        }

        foreach ($this->shieldSlots as $slot) {
            if (!$slot->isEquipped()) {
                yield $slot;
            }
        }
    }

    /**
     * @return ShieldSlot[]
     */
    public function getEquippedShieldSlots()
    {
        foreach ($this->shieldSlots as $shieldSlot) {
            if ($shieldSlot->isEquipped()) {
                yield $shieldSlot;
            }
        }
    }
}
