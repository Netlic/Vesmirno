<?php

namespace Vesmirno\Traits\Command;

use Vesmirno\Translator\Translator;

/**
 * Trait God
 * @package Vesmirno\Traits\Command
 * @property Translator $t
 */
trait God
{
    /**
     * @return string
     */
    public function commandGodCreate(): string
    {
        return $this->t->__('Created new');
    }
}