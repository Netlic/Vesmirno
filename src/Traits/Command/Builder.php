<?php

namespace Vesmirno\Traits\Command;

use Vesmirno\Translator\Translator;

/**
 * Class Builder
 * @package Vesmirno\Traits\Command
 * @property Translator $t
 */
trait Builder
{
    public function commandBuilderBuild()
    {
        return $this->t->__('commenced_construction');
    }

    public function commandUnknownActivity()
    {
        return $this->t->__('no_clue_what_to_do');
    }
}