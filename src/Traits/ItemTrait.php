<?php

namespace Vesmirno\Traits;

trait ItemTrait
{

    protected function getItemSlot(string $class)
    {
        $vars = get_object_vars($this);
        foreach ($vars as $var) {
            if (is_array($var) && !empty($var)) {
                if (get_class(reset($var)) === $class) {
                    return $var;
                }
            }
        }

        return [];
    }

    public function getItemSlotsCount(string $class = null, array $itemSlot = [])
    {
        if (!empty($itemSlot)) {
            return count($itemSlot);
        }

        if (!$class) {
            return 0;
        }

        return count($this->getItemSlot($class));
    }

    public function getEmptyItemSlots(string $class = null, array $itemSlot = [])
    {
        $slots = [];

        if (!empty($itemSlot)) {
            $slots = $itemSlot;
        }

        if (empty($slots) && $class) {
            $slots = $this->getItemSlot($class);
        }

        foreach ($slots as $slot) {
            if (!$slot->isEquipped()) {
                yield $slot;
            }
        }
    }

    /* public function getEquippedWeaponSlots(string $class = null, array $itemSlot = []) {
      foreach ($this->weaponSlots as $weaponSlot)
      {
      if ($weaponSlot->isEquippedWithWeapon()) {
      yield $weaponSlot;
      }
      }
      } */
}
