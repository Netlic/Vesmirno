<?php

namespace Vesmirno\Traits\Job;

use Vesmirno\Interfaces\Tools\ToolInterface;

trait JobTrait
{
    /** @var int */
    protected int $jobSkill;

    /** @var ToolInterface[] */
    protected array $toolBox = [];

    /**
     * @return int
     */
    public function getJobSkill()
    {
        return $this->jobSkill;
    }

    /**
     * @param int $jobSkill
     */
    public function setJobSkill(int $jobSkill)
    {
        $this->jobSkill = $jobSkill;
    }

    /**
     * @param ToolInterface $tool
     */
    public function addTool(ToolInterface $tool)
    {
        $this->toolBox[] = $tool;
    }
}