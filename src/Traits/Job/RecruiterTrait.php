<?php

namespace Vesmirno\Traits\Job;

use Vesmirno\Game\Staff\Jobs\JobInterface;

trait RecruiterTrait
{
    public function findPersonForJob(JobInterface $job)
    {
        $job->requirements();
    }
}