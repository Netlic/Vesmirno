<?php

namespace Vesmirno\Traits\Job;

use Vesmirno\Game\Objects\Growers\Plants\Trees\TreeInterface;
use Vesmirno\Game\Objects\Resources\Wood\Wood;
use Vesmirno\Interfaces\Material\CutableInterface;
use Vesmirno\Interfaces\Tools\CutInterface;

trait WoodcutterTrait
{
    /** @var CutInterface|null */
    protected ?CutInterface $tool;

    /**
     * @param CutableInterface $cuttableMaterial
     * @param CutInterface|null $tool
     */
    public function cut(CutableInterface $cuttableMaterial, CutInterface $tool = null)
    {

    }

    public function createLogs(CutableInterface $wood)
    {

    }

    public function createPlanks()
    {

    }

    public function createFirewood()
    {

    }
}