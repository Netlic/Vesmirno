<?php

namespace Vesmirno\Traits;

use Vesmirno\Slots\VehicleSlot\WeaponSlot;

/**
 *
 * @author brano
 */
trait WeaponTrait
{

    /** @var WeaponSlot[] */
    protected $weaponSlots = [];

    public function getWeaponSlots()
    {
        return $this->weaponSlots;
    }

    /**
     * Gets the weapon slots available on vehicle
     * @return int
     */
    public function getWeaponSlotsCount() : int
    {
        if (method_exists($this, 'getItemSlotsCount')) {
            return $this->getItemSlotsCount(null, $this->weaponSlots);
        }

        return count($this->weaponSlots);
    }

    /**
     * 
     * @return WeaponSlot[]
     */
    public function getEmptyWeaponSlots()
    {
        if (method_exists($this, 'getEmptyItemSlots')) {
            return $this->getEmptyItemSlots(null, $this->weaponSlots);
        }

        foreach ($this->weaponSlots as $slot) {
            if (!$slot->isEquipped()) {
                yield $slot;
            }
        }
    }

    /**
     * @return WeaponSlot[]
     */
    public function getEquippedWeaponSlots()
    {
        foreach ($this->weaponSlots as $weaponSlot) {
            if ($weaponSlot->isEquipped()) {
                yield $weaponSlot;
            }
        }
    }

    /**
     * 
     * @return int
     */
    public function fire()
    {
        var_dump('Your '  . $this->vehicleType() . ' attempts to fire!');
        $totalDamage = 0;
        $weaponsFired = 0;
        foreach($this->getEquippedWeaponSlots() as $weaponSlot) {
            $totalDamage += $weaponSlot->inspectItem()->fire();
            $weaponsFired++;
        }
        
        if ($weaponsFired == 0) {
            var_dump('Unfortunatelly, your ' . $this->vehicleType() . ' has no weapons equipped.');
        }

        return $totalDamage;
    }
}
