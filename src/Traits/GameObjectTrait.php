<?php

namespace Vesmirno\Traits;

trait GameObjectTrait
{
    /** @var string */
    protected $objectId;

    /**
     * @return string
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param string $objectId
     * @return $this
     */
    public function setObjectId(string $objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }
}