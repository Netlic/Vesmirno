<?php

namespace Vesmirno\Traits;

use Vesmirno\Game\Game;
use Vesmirno\Translator\Translator;

trait TranslatorTrait
{
    /** @var Translator */
    protected $t;

    /**
     * Initiates local class attribute $this->t with `Vesmirno\Translator\Translator instance`
     */
    protected function initiateTranslator()
    {
        $this->t = Game::translator();
    }
}