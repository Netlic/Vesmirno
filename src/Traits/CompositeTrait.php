<?php

namespace Vesmirno\Traits;

trait CompositeTrait
{
    /**
     * Creates instance of main composite class
     * @param string $compositeClass
     * @param array $compositePartClasses
     * @param $localAttribute
     */
    protected function instantiateData(string $compositeClass, array $compositePartClasses, &$localAttribute)
    {
        $classes = [];
        foreach ($compositePartClasses as $key => $systemDataClass) {
            $classes[$key] = new $systemDataClass();
        }

        $localAttribute = new $compositeClass($classes);
    }
}