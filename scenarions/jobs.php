<?php

use Vesmirno\Game\Staff\Jobs\Job;
use Vesmirno\Game\Staff\People\Human;
use Vesmirno\Traits\GenderTrait;
use Vesmirno\Traits\Job\JobTraitOld;
use Vesmirno\Traits\Job\WoodcutterTraitOld;

require dirname(__DIR__). '/vendor/autoload.php';

$competences = JobTraitOld::getCompetences(JobTraitOld::WOODCUTTER, 'Trait');
//$human = new Human(Human::GENDER_FEMALE);
$job = new class($competences) extends Job {
    use GenderTrait;

    /** @var WoodcutterTraitOld */
    protected $jobCompetences;

    /**
     * @return int[]
     */
    public function requirements()
    {
        return ['age' => 18, 'strength' => 3];
    }

    public function jobDescription(): string
    {
        return 'Lumberjack';
    }

    public function at()
    {
        // TODO: Implement at() method.
    }
};

echo $job->jobDescription();
var_dump($job->requirements(), $job->competences());
echo $job;
//$human->setJob($job);