<?php

use Vesmirno\Game\Game;
use Vesmirno\Helper\Config\GameConfigHelper;

require dirname(__DIR__). '/vendor/autoload.php';
$defaultConfig = file_get_contents(dirname(__DIR__) . '/default.game.config.json');

$game = new Game();
$config = new GameConfigHelper($defaultConfig);
$game->start($config);
$game->awaitCommand();
